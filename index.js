var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

io.set('origins', '*:*');


app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on("connection", socket => {
	console.log('socket connected');

	socket.on("notification", msg => {
		console.log('notification call')
		io.emit("notification", msg)
	})
	socket.on("message", msg => {
		console.log('message call')
		io.emit("message", msg)
	})
	socket.on("globalMessage", msg => {
		console.log('globalMessage call')
		io.emit("globalMessage", msg)
	})
	socket.on('disconnect', () => {
	    console.log('socket disconnected');
	});
})

http.listen(3000, () => {
  console.log('listening on *:3000');
});
